package com.realestatefounds.founds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealestatefoundsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealestatefoundsApplication.class, args);
	}

}
