package com.realestatefounds.founds.exceptions;

import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.swing.text.DateFormatter;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Formatter;

import static com.realestatefounds.founds.constants.ConstantsFounds.ERROR_HANDLER;

@ControllerAdvice
public class Handling extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

//   public Handling(MessageSource messageSource){
//       this.messageSource = messageSource;
//   }


    @ExceptionHandler(value = ExceptionFiis.class)
    public ResponseEntity<Object> handlerTickersNotFound(ExceptionFiis exceptionFiis , WebRequest webRequest){

         var status = HttpStatus.NOT_FOUND;
         var msgException = MsgException
                 .builder()
                 .localDateTime(OffsetDateTime.now())
                 .error(ERROR_HANDLER)
                 .status(status.value())
                 .message(exceptionFiis.getMessage())
                 .developerMessage(exceptionFiis.getClass().getName())
                 .build();
         return handleExceptionInternal(exceptionFiis,msgException,new HttpHeaders(),status,webRequest);
    }

//
//    @Override
//    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//
//        var msgs = new ArrayList<MsgException.Msg>();
//
//        for (ObjectError objectError : ex.getBindingResult().getAllErrors()){
//            String nomeCampos = ((FieldError) objectError).getField();
//            String mensagens = "DFNEF";// messageSource.getMessage(objectError , LocaleContextHolder.getLocale());
//
//            msgs.add(new MsgException.Msg(nomeCampos , mensagens));
//
//        }
//
//        var msgException = new MsgException();
//
//        msgException.setStatus(status.value());
//        msgException.setTitle("Houve um problema tente novamente ou em entre em contato com o administrador");
//        msgException.setOffsetDateTime(OffsetDateTime.now());
//
//        msgException.setMessage(msgs);
//
//        return super.handleExceptionInternal(ex,msgException,headers,status,request);
//    }
//


}
