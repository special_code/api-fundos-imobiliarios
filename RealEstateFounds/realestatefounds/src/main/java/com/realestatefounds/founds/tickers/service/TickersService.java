package com.realestatefounds.founds.tickers.service;


import com.realestatefounds.founds.exceptions.ExceptionFiis;
import com.realestatefounds.founds.tickers.model.Tickers;
import com.realestatefounds.founds.tickers.repository.TickersRepository;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


@Service
public class TickersService {


    @Autowired
     private TickersRepository tickersRepository;


     public TickersService(TickersRepository tickersRepository){
         this.tickersRepository = tickersRepository;
     }


     public List<Tickers> getAll(){
         return (List<Tickers>) tickersRepository.findAll();
     }

     public Tickers getIdTickers(Long id) throws ExceptionFiis {

       Tickers tickers =   tickersRepository.findById(id)
               .orElseThrow( () -> new ExceptionFiis("Ticker NOT FOUND " + id));

       return tickers;

     }



    }

