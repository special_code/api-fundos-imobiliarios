package com.realestatefounds.founds.abstractmapper;


import org.springframework.stereotype.Component;

import java.util.List;

@Component
public abstract class AbstractMethodsMapper<T,E>{

  protected abstract T toDto(E e);
  protected abstract List<T> toCollectDTO(List<E> e);


}
