package com.realestatefounds.founds.founds.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoundsDTO {

    //    @ManyToMany
    @Column(name = "id_fundos_razao")
    private Long idFuncaoRazao;

    @Column(name = "fundo_fiis")
    private String fundoFiis;
}
