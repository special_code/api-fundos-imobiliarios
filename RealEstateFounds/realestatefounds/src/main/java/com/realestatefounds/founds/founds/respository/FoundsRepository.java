package com.realestatefounds.founds.founds.respository;

import com.realestatefounds.founds.founds.model.Founds;
import org.springframework.data.repository.CrudRepository;

public interface FoundsRepository extends CrudRepository<Founds,Long> {
}
