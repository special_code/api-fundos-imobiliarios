package com.realestatefounds.founds.tickers.controller;


import com.realestatefounds.founds.abstractmapper.AbstractMethodsMapper;
import com.realestatefounds.founds.exceptions.ExceptionFiis;
import com.realestatefounds.founds.tickers.model.dto.TickerDTO;
import com.realestatefounds.founds.tickers.model.Tickers;
import com.realestatefounds.founds.tickers.service.TickersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_TICKERS;
import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_TICKERS_ID;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
//@RequestMapping(value = "/tickers" , produces = "application/hal+json")
public class TickersController extends AbstractMethodsMapper<TickerDTO, Tickers>{


     @Autowired
     private TickersService tickersService;

     @Autowired
     private ModelMapper modelMapper;



//
//    public TickersController(TickersService tickersService){
//        this.tickersService = tickersService;
//    }
//    public TickersController(ModelMapper modelMapper) {this.modelMapper = modelMapper;}

//    public TickersController(){
//
//    }

//    @RequestMapping("/api/v1/")
//    public HttpEntity<TickerDTO> responseHetoas(@RequestParam(name = "codigo" ,defaultValue = "codigos_dos_fundos") String name ){
//        TickerDTO tickerDTO = new TickerDTO(String.format(name));
//
//        tickerDTO.add(linkTo(methodOn(TickersController.class).responseHetoas(name)).withSelfRel());
//        return new ResponseEntity<>(tickerDTO, HttpStatus.OK);
//    }


    @GetMapping(LIST_TICKERS)
    public List<TickerDTO> getListTickers(){
        return toCollectDTO(tickersService.getAll());
    }

    @GetMapping(LIST_TICKERS_ID)
    public TickerDTO getListTickersId( @PathVariable Long id){
       return toDto(tickersService.getIdTickers(id));
    }

    @Override
    protected TickerDTO toDto(Tickers tickers) {
        return modelMapper.map(tickers, TickerDTO.class);
    }

    @Override
    protected List<TickerDTO> toCollectDTO(List<Tickers> tickers) {
        return tickers.stream().map(param -> toDto(param)).collect(Collectors.toList());
    }





}
