package com.realestatefounds.founds.exceptions;

import lombok.*;

import java.time.OffsetDateTime;


@Builder
@Getter
@AllArgsConstructor
public class MsgException {


    private OffsetDateTime localDateTime;
    private String error;
    private Integer status;
    private String message;
    private String developerMessage;




}
