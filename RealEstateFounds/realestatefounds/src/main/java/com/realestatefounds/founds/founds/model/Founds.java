package com.realestatefounds.founds.founds.model;


import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "fundos")
public class Founds implements Serializable {
    private final static long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id_fundos")
    private Long id;


//    @ManyToMany
    @Column(name = "id_fundos_razao")
    private Long idFuncaoRazao;

    @Column(name = "fundo_fiis")
    private String fundoFiis;

}
