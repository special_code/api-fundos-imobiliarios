package com.realestatefounds.founds.tickers.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tickers")
public class Tickers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "id_tickers")
    private Long id;


//    @ManyToMany
    @Column(name = "id_tickers_razao")
    private Long idTickersRazao;

    @Column(name = "codigo")
    @NotNull
    private String codigo;


}
