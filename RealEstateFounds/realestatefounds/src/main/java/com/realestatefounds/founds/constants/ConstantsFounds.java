package com.realestatefounds.founds.constants;

public  class ConstantsFounds {

  public static final String LIST_FOUNDS = "/list/founds";
  public static final String LIST_FOUNDS_ID = "/list/founds/{id}";
  public static final String LIST_NAMES_FOUNDS = "/list/names";
  public static final String LIST_NAMES_ID= "/list/names/{id}";
  public static final String LIST_TICKERS = "/list/tickers";
  public static final String LIST_TICKERS_ID = "/list/tickers/{id}";
  public static final String SQL_QUERY = "SELECT t FROM tickers WHERE t.codigo LIKE :codigoName";
  public static final String ERROR_HANDLER= "Resource not found";

}
