package com.realestatefounds.founds.nomesfundos.repository;


import com.realestatefounds.founds.nomesfundos.model.NameFounds;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NameFoundsRepository extends  CrudRepository<NameFounds , Long> {



}
