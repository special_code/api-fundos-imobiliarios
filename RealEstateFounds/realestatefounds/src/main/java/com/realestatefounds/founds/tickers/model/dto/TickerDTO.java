package com.realestatefounds.founds.tickers.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class TickerDTO extends RepresentationModel<TickerDTO> {

    @Column(name = "id_tickers")
    private Long id;

    //    @ManyToMany
    @Column(name = "id_tickers_razao")
    private Long idTickersRazao;

    @Column(name = "codigo")
    @NotNull
    private String codigo;

//    private String content;
//
//    @JsonCreator
//    public TickerDTO(@JsonProperty("content") String content){
//        this.content = content;
//    }

}
