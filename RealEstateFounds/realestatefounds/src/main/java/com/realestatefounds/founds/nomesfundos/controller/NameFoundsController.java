package com.realestatefounds.founds.nomesfundos.controller;

import com.realestatefounds.founds.abstractmapper.AbstractMethodsMapper;
import com.realestatefounds.founds.nomesfundos.model.NameFounds;
import com.realestatefounds.founds.nomesfundos.model.dto.NameFoundsDTO;
import com.realestatefounds.founds.nomesfundos.service.NameFoundsService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_NAMES_FOUNDS;
import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_NAMES_ID;

@RestController
public class NameFoundsController extends AbstractMethodsMapper<NameFoundsDTO , NameFounds> {

    @Autowired
    private NameFoundsService nameFoundsService;
    @Autowired
    private ModelMapper modelMapper;


    @GetMapping(LIST_NAMES_FOUNDS)
    public List<NameFoundsDTO> getAll(){
        return toCollectDTO(nameFoundsService.getAll());
    }

    @GetMapping(LIST_NAMES_ID)
    public  NameFoundsDTO getNameFoundsId(@PathVariable Long id){
        return toDto(nameFoundsService.getAllId(id));
    }

    @Override
    protected NameFoundsDTO toDto(NameFounds nameFounds) {
        return modelMapper.map(nameFounds, NameFoundsDTO.class) ;
    }

    @Override
    protected List<NameFoundsDTO> toCollectDTO(List<NameFounds> nameFounds) {
        return nameFounds.stream().map( param ->  toDto(param)).collect(Collectors.toList());
    }
}
