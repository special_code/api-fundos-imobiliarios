package com.realestatefounds.founds.founds.service;


import com.realestatefounds.founds.founds.model.Founds;
import com.realestatefounds.founds.founds.respository.FoundsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FoundService {


   @Autowired
    private FoundsRepository foundsRepository;


   public List<Founds> getAllFounds(){
       return (List<Founds>) foundsRepository.findAll();
   }

    public Founds getAllFoundsId(Long id) {
      Founds founds =  foundsRepository.findById(id)
              .orElseThrow(() -> new RuntimeException("NAO FOI POSSIVEL ACHAR ESTE ID"));

      return founds;
    }
}
