package com.realestatefounds.founds.tickers.repository;


import com.realestatefounds.founds.tickers.model.Tickers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TickersRepository extends CrudRepository<Tickers , Long>{
}
