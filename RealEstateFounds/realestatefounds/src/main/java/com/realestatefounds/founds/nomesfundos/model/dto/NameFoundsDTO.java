package com.realestatefounds.founds.nomesfundos.model.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NameFoundsDTO {




    @Column(name = "razao_social" , nullable = false)
    @NotNull
    private String razaoSocial;
}
