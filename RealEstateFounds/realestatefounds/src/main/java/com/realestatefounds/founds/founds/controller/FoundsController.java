package com.realestatefounds.founds.founds.controller;


import com.realestatefounds.founds.abstractmapper.AbstractMethodsMapper;
import com.realestatefounds.founds.founds.model.Founds;
import com.realestatefounds.founds.founds.model.dto.FoundsDTO;
import com.realestatefounds.founds.founds.service.FoundService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_FOUNDS;
import static com.realestatefounds.founds.constants.ConstantsFounds.LIST_FOUNDS_ID;

@RestController
public class FoundsController extends AbstractMethodsMapper<FoundsDTO, Founds> {


    @Autowired
    private FoundService foundService;

    @Autowired
    ModelMapper modelMapper;


    @GetMapping(LIST_FOUNDS)
    public List<FoundsDTO> getAllFounds(){
        return toCollectDTO(foundService.getAllFounds());
    }

    @GetMapping(LIST_FOUNDS_ID)
    public FoundsDTO getFoundId(@PathVariable Long id){
         return toDto(foundService.getAllFoundsId(id));
    }

    @Override
    protected FoundsDTO toDto(Founds founds) {
        return modelMapper.map(founds, FoundsDTO.class);
    }

    @Override
    protected List<FoundsDTO> toCollectDTO(List<Founds> founds) {
        return founds.stream().map( param -> toDto(param)).collect(Collectors.toList());
    }
}
