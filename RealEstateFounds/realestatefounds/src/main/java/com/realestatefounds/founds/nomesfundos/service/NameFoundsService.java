package com.realestatefounds.founds.nomesfundos.service;

import com.realestatefounds.founds.nomesfundos.model.NameFounds;
import com.realestatefounds.founds.nomesfundos.repository.NameFoundsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NameFoundsService {

     @Autowired
     private NameFoundsRepository nameFoundsRepository;

     public List<NameFounds> getAll(){
          return (List<NameFounds>) nameFoundsRepository.findAll();
     }

     public NameFounds getAllId(Long id){
      NameFounds nameFounds =  nameFoundsRepository.findById(id)
              .orElseThrow(() -> new RuntimeException("NAO FOI POSSIVEL ACHAR ESTE ELEMENTO COM ESTA ID"));
      return nameFounds;
     }

}
