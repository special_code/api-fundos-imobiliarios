package com.realestatefounds.founds.nomesfundos.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "nomes_fiis")
public class NameFounds implements Serializable {

    private static final long serialVersionUID=1L;


    @Id
    @GeneratedValue
    @Column(name = "id_razao")
    private Long id;

    @Column(name = "razao_social" , nullable = false)
    @NotNull
    private String razaoSocial;
}
