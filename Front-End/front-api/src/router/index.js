import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home,
  },

  {
    path: '/about',
    name: 'About',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },

  {
    path: '/tickers',
    name: 'Tickers',
    component: () => import('../views/Tickers.vue'),
  },


  {
     path: '/founds',
     name: 'Founds',

     component:()=> import('../views/Founds.vue'),

  },

  {
    path: '/name-founds',
    name:'NameFounds',

    component:()=> import('../views/NameFounds.vue'),
  }

];

const router = new VueRouter({
  routes,
});
export default router;
